# Dépannage de votre cluster K8s

------------

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

# Intro

Nous allons parler du dépannage de votre cluster Kubernetes. Voici un aperçu rapide de ce dont nous allons discuter :

- **Serveur API Kubernetes et dépannage**
- **Vérification de l'état des nœuds**
- **Vérification des services Kubernetes**
- **Vérification des pods système Kubernetes**
- **Démonstration pratique**

# Serveur API Kubernetes

L'une des premières choses dont vous devez être conscient en matière de dépannage est le serveur API Kubernetes. Nous en avons déjà parlé, mais vous vous souvenez peut-être que le serveur API Kubernetes est notre principal moyen d'interagir avec le cluster Kubernetes. Donc, si le serveur API Kubernetes est en panne, vous ne pourrez pas utiliser `kubectl` pour interagir avec le cluster, car `kubectl` dépend de `kube-apiserver`.

illustration : 
```bash
cloud_user@k8s_control:~kubectl get nodes
The connection to the server localhost:6443 was refused - did you specify the right host or port?
```

Si vous essayez d'interagir avec votre cluster Kubernetes en utilisant `kubectl` et que vous obtenez un message d'erreur indiquant que la connexion au serveur a été refusée, cela pourrait signifier que le serveur API est en panne. Maintenant, si votre `kubeconfig` n'est pas configuré correctement, vous pourriez également obtenir un message d'erreur similaire, mais en supposant que `kubeconfig` soit configuré correctement, cela pourrait signifier que vous avez un problème avec votre serveur API, ce qui va compliquer votre dépannage.

Quelques solutions possibles si `kube-apiserver` est en panne pourraient inclure de vérifier que les services Docker et `kubelet` sont en cours d'exécution sur vos nœuds de plan de contrôle.

# Vérification de l'état des nœuds

Si le serveur API Kubernetes est sain et que nous pouvons interagir avec notre cluster en utilisant `kubectl`, la prochaine étape est de vérifier l'état de nos nœuds. Si nous avons un problème que nous essayons de dépanner, nous devons vérifier l'état des nœuds pour voir si l'un d'eux rencontre des problèmes. Vous pouvez utiliser la commande simple `kubectl get nodes` pour voir l'état général de chaque nœud. Vous pouvez également utiliser `kubectl describe node` pour obtenir plus d'informations sur les nœuds qui ne sont pas dans l'état prêt.

```shell
kubectl get nodes
kubectl describe node <node-name>
```

# Vérification des services Kubernetes

Si un nœud a des problèmes, cela peut être dû à un service Kubernetes en panne sur ce nœud. Chaque nœud exécute `kubelet`, et bien sûr, nous exécutons également Docker en tant que service. Vous pouvez utiliser `systemctl status` sur l'un de ces services pour afficher leur état, et si un service est en panne, vous pouvez essayer de le démarrer et de l'activer.

```shell
sudo systemctl status kubelet
sudo systemctl start kubelet
sudo systemctl enable kubelet
```

# Vérification des pods système

Dans un cluster `kubeadm`, plusieurs composants Kubernetes s'exécutent en tant que pods dans l'espace de noms `kube-system`. Vous pouvez vérifier l'état de ces composants simplement avec `kubectl get pods` et `kubectl describe pod` dans cet espace de noms `kube-system`.

```shell
kubectl get pods -n kube-system
kubectl describe pod <pod-name> -n kube-system
```

# Démonstration pratique

Voyons maintenant à quoi cela ressemble dans un environnement Kubernetes réel. Nous nous connectons à notre serveur de plan de contrôle Kubernetes, et nous allons faire un rapide `kubectl get nodes`.

```shell
kubectl get nodes
```

Nous pouvons voir nos trois nœuds et leur état. Si l'un de ces nœuds avait un état autre que prêt, nous saurions qu'il a probablement un problème et que nous devons l'examiner de plus près.

Nous pouvons utiliser `kubectl describe node` pour obtenir plus d'informations sur chaque nœud. Par exemple, nous pouvons vérifier la section **Conditions** dans la sortie de `kubectl describe`, qui peut fournir des informations utiles si quelque chose ne va pas avec le nœud.

```shell
kubectl describe node <node-name>
```

Nous pouvons également vérifier l'état de notre service `kubelet` avec `sudo systemctl status kubelet`.

```shell
sudo systemctl status kubelet
```

Nous pouvons voir que notre service `kubelet` est actif et en cours d'exécution, et qu'il est activé pour démarrer automatiquement au démarrage du serveur.

Passons maintenant à un nœud de travail et voyons ce qui se passe si quelque chose ne va pas avec l'un de nos services. Sur le nœud de travail, nous allons arrêter et désactiver `kubelet` avec :

```shell
sudo systemctl stop kubelet
sudo systemctl disable kubelet
```

Si nous vérifions à nouveau l'état avec `sudo systemctl status kubelet`, nous pouvons voir qu'il est inactif et désactivé.

De retour sur notre nœud de plan de contrôle, si nous relançons `kubectl get nodes`, nous verrons que notre nœud de travail est maintenant dans un état **NotReady** après un court laps de temps. C'est parce que `kubelet` ne se connecte plus au serveur API Kubernetes, ce qui indique au plan de contrôle qu'il y a un problème avec ce nœud.

Pour résoudre ce problème, nous retournons sur le nœud de travail, nous redémarrons et réactivons `kubelet` avec :

```shell
sudo systemctl start kubelet
sudo systemctl enable kubelet
```

Si nous vérifions l'état à nouveau, nous verrons que `kubelet` est actif et activé. De retour sur le nœud de plan de contrôle, si nous relançons `kubectl get nodes`, nous verrons que le nœud est à nouveau prêt.

Enfin, nous vérifions les pods système Kubernetes avec :

```shell
kubectl get pods -n kube-system
```

Nous pouvons voir différents pods système ici, comme `kube-proxy`, `kube-controller-manager`, `kube-apiserver`, `dns`, et d'autres. Si l'un de ces pods n'est pas en état de fonctionnement, cela pourrait être la cause de problèmes dans votre cluster.

En résumé, nous avons parlé du serveur API Kubernetes en relation avec le dépannage, de la vérification de l'état des nœuds, de la vérification des services Kubernetes et des pods système, et nous avons fait une démonstration pratique de certains de ces concepts. C'est tout pour cette leçon .


# Reférence

https://kubernetes.io/docs/tasks/debug/

https://kubernetes.io/docs/tasks/debug/debug-cluster/